from django.shortcuts import render
from .models import TodoList


def todo_list_list(request):
    todo_lists = TodoList.objects.all()
    return render(
        request, "todos/todo_list_list.html", {"todo_lists": todo_lists}
    )
